package dimitrije.culjkovic.fon.engineeringproject.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "examapplication")
public class ExamApplication implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 193309042020L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name  = "examapp_id")
	private Long id;
	@ManyToOne
	@JoinColumn (name = "student_id")
	private Student student;
	@ManyToOne
	@JoinColumn(name = "exam_id")
	private Exam exam;
	
	
	public ExamApplication() {

	}


	public ExamApplication(Long id, Student student, Exam exam) {
		super();
		this.id = id;
		this.student = student;
		this.exam = exam;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Student getStudent() {
		return student;
	}


	public void setStudent(Student student) {
		this.student = student;
	}


	public Exam getExam() {
		return exam;
	}


	public void setExam(Exam exam) {
		this.exam = exam;
	}


	@Override
	public String toString() {
		return "ExamApplication [id=" + id + ", student=" + student + ", exam=" + exam + "]";
	}
	
	
	
	
	
}
