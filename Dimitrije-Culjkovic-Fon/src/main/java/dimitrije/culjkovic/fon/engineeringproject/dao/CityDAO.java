package dimitrije.culjkovic.fon.engineeringproject.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dimitrije.culjkovic.fon.engineeringproject.domain.City;
@Repository
public interface CityDAO extends JpaRepository<City, Long>{

	
}
