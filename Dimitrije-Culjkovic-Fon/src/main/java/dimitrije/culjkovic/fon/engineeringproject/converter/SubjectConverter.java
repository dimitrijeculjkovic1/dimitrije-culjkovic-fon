package dimitrije.culjkovic.fon.engineeringproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import dimitrije.culjkovic.fon.engineeringproject.domain.Subject;
import dimitrije.culjkovic.fon.engineeringproject.dto.SubjectDto;

@Component
public class SubjectConverter {

 public SubjectDto entityToDto(Subject subject) {
	 return new SubjectDto(subject.getId(), subject.getName(), subject.getDescription(), subject.getYearOfStudy(), subject.getSemester());
 }
 
 public Subject dtoToEntity(SubjectDto subjectDto) {
	 return new Subject(subjectDto.getId(), subjectDto.getName(), subjectDto.getDescription(),
			 subjectDto.getYearOfStudy(), subjectDto.getSemester());
 }
 
 public List<SubjectDto> entityToDtoList(List<Subject> subjectList){
	 List<SubjectDto> dtoList = new ArrayList<SubjectDto>();
	 for (Subject subject : subjectList) {
		dtoList.add(entityToDto(subject));
	}
	 return dtoList;
 }
 
 public List<Subject> dtoToEntityList(List<SubjectDto> dtoList) {
		List<Subject> subjectList = new ArrayList<Subject>();
		for(SubjectDto subjectDto: dtoList) {
			subjectList.add(dtoToEntity(subjectDto));
		}
		return subjectList;
	}
 
}
