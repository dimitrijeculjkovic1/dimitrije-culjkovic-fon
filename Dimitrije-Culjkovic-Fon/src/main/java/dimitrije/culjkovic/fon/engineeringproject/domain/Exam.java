package dimitrije.culjkovic.fon.engineeringproject.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "exam")
public class Exam implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 151708042020L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "exam_id")
	private Long id;
	@ManyToOne
	@JoinColumn(name = "professor_id")
	private Professor professor;
	@ManyToOne
	@JoinColumn(name = "subject_id")
	private Subject subject;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "exam_date")
	private Date examDate;

	public Exam() {

	}

	public Exam(Long id, Professor professor, Subject subject, Date examDate) {
		super();
		this.id = id;
		this.professor = professor;
		this.subject = subject;
		this.examDate = examDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

	@Override
	public String toString() {
		return "Exam [id=" + id + ", professor=" + professor + ", subject=" + subject + ", examDate=" + examDate + "]";
	}
	
	
	
	
	

}
