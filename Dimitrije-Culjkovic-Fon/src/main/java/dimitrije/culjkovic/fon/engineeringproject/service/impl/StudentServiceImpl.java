	package dimitrije.culjkovic.fon.engineeringproject.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dimitrije.culjkovic.fon.engineeringproject.converter.StudentConverter;
import dimitrije.culjkovic.fon.engineeringproject.dao.StudentDAO;
import dimitrije.culjkovic.fon.engineeringproject.domain.Student;
import dimitrije.culjkovic.fon.engineeringproject.dto.StudentDto;
import dimitrije.culjkovic.fon.engineeringproject.service.StudentService;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {
	
	
	private StudentDAO studentDAO;

	private StudentConverter studentConverter;
	@Autowired	
	public StudentServiceImpl(StudentDAO studentDAO, StudentConverter studentConverter) {
		this.studentDAO = studentDAO;
		this.studentConverter = studentConverter;
	}
	@Override
	public List<StudentDto> allStudents() {
	
		List<Student> list = studentDAO.findAll();
		List<StudentDto> students = studentConverter.entityToDtoList(list);
		
		return students;
	}
	@Override
	public void saveStudent(StudentDto studentDto) {
		studentDAO.save(studentConverter.dtoToEntity(studentDto));
		
	}
	@Override
	public StudentDto findById(Long id) {
	Optional<Student> find = studentDAO.findById(id);
	
		return studentConverter.entityToDto(find.get());
	}
	@Override
	public void deleteStudent(Long id) {
	 studentDAO.deleteById(id);
		
	}


	
	


}
