package dimitrije.culjkovic.fon.engineeringproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import dimitrije.culjkovic.fon.engineeringproject.domain.City;
import dimitrije.culjkovic.fon.engineeringproject.domain.Professor;
import dimitrije.culjkovic.fon.engineeringproject.domain.Title;
import dimitrije.culjkovic.fon.engineeringproject.dto.CityDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.ProfessorDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.StudentDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.TitleDto;

@Component
public class ProfessorConverter {
	
	public ProfessorDto entityToDto(Professor professor) {
		return new ProfessorDto(professor.getId(),professor.getFirstName(), professor.getLastName(), professor.getEmail(), 
				professor.getAddress(), new CityDto(professor.getCity().getId(), professor.getCity().getCityNumber(),
				professor.getCity().getCityName()),professor.getPhone(), professor.getReelectionDate(),
				new TitleDto(professor.getTitle().getId(), professor.getTitle().getTitleNumber(),
						professor.getTitle().getTitleName()));
	}
	
	public Professor DtoToEntity(ProfessorDto professorDto) {
		return new Professor(professorDto.getId(),professorDto.getFirstName(),professorDto.getLastName(),professorDto.getEmail(),
				professorDto.getAddress(), new City(professorDto.getCityDto().getId(),professorDto.getCityDto().getCityNumber(),
						professorDto.getCityDto().getCityName()), professorDto.getPhone(), professorDto.getReelectionDate(),
				new Title(professorDto.getTitleDto().getId(),professorDto.getTitleDto().getTitleNumber(),professorDto.getTitleDto().getTitleName()));
	}
	
	public List<ProfessorDto> entityToDtoList(List<Professor> professors) {
		List<ProfessorDto> professorsDto = new ArrayList<ProfessorDto>();
		for (Professor professor : professors) {
			professorsDto.add(entityToDto(professor));
		}
		return professorsDto;
	}
	
	public List<Professor> dtoToEntityList(List<ProfessorDto> professorsDto){
		List<Professor> professors = new ArrayList<Professor>();
		for(ProfessorDto professorDto : professorsDto) {
			professors.add(DtoToEntity(professorDto));
		}
		return professors;
	}

}
