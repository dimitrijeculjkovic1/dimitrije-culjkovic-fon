package dimitrije.culjkovic.fon.engineeringproject.service;

import java.util.List;

import dimitrije.culjkovic.fon.engineeringproject.dto.ProfessorDto;


public interface ProfessorService {

	public List<ProfessorDto> getProfessors();
	
	public void saveProfessor(ProfessorDto professorDto);
	
	public ProfessorDto findById(Long id);
	
	public void delete(Long id);
	
}
