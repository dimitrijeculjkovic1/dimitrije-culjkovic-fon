package dimitrije.culjkovic.fon.engineeringproject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dimitrije.culjkovic.fon.engineeringproject.dto.CityDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.ProfessorDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.StudentDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.TitleDto;
import dimitrije.culjkovic.fon.engineeringproject.service.CityService;
import dimitrije.culjkovic.fon.engineeringproject.service.ProfessorService;
import dimitrije.culjkovic.fon.engineeringproject.service.TitleService;

@Controller
@RequestMapping("/professor")
public class ProfessorController {
	
	private final ProfessorService professorService;
	private final CityService cityService;
	private final TitleService titleService;
	
	@Autowired
	public ProfessorController(ProfessorService professorService, CityService cityService, TitleService titleService) {
		super();
		this.professorService = professorService;
		this.cityService = cityService;
		this.titleService = titleService;
	}
	
	@GetMapping("/home")
	public String home() {
		return "index";
	}
	

	@ModelAttribute(name = "cities")
	private List<CityDto> getCities() {
		return cityService.getAll();
	}
	
	@ModelAttribute(name = "titles")
	private List<TitleDto> getTitles() {
		return titleService.getAll();
	}
	
	
	@GetMapping("/list")
	public String ProfessorList(Model model) {
		List<ProfessorDto> professors = professorService.getProfessors();
		
		model.addAttribute("professors" , professors);
		
		return "professor-list";
	}

	

	@GetMapping("/add")
	public String addProfessor(Model model) {
		ProfessorDto professorDto = new ProfessorDto();
		model.addAttribute("professorDto" , professorDto);
		return "professor-form";
		
	}

	@PostMapping("/save")
	public String saveProfessor(@Valid @ModelAttribute("professorDto") ProfessorDto professorDto , BindingResult theBindingResult) {
		System.out.println(professorDto);
        
		if(theBindingResult.hasErrors()) {
			return "professor-form";
		}else {
			professorService.saveProfessor(professorDto);
			return "redirect:/professor/list";
		}
	}
	
	@GetMapping("/update")
	public String update(@RequestParam("id") Long id , Model model) {
		ProfessorDto professorDto = professorService.findById(id);
		
		model.addAttribute("professorDto" , professorDto);
		
		return "professor-form";
	}
	
	@GetMapping("/details")
	public String details(@RequestParam("id") Long id , Model model) {
		ProfessorDto professorDto = professorService.findById(id);
		
		model.addAttribute("professorDto" , professorDto);
		
		return "professor-details";
	}
	
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
		professorService.delete(id);
		
		return "redirect:/professor/list";
	}

	
	
	
	
	
	

}
