package dimitrije.culjkovic.fon.engineeringproject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dimitrije.culjkovic.fon.engineeringproject.dao.StudentDAO;
import dimitrije.culjkovic.fon.engineeringproject.dto.CityDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.StudentDto;
import dimitrije.culjkovic.fon.engineeringproject.service.CityService;
import dimitrije.culjkovic.fon.engineeringproject.service.StudentService;
@Controller
@RequestMapping("/student")
public class StudentController {
	
	//potrebno je ubaciti studentservice
		private final StudentService studentService;
		private final CityService cityService;

		@Autowired
		public StudentController(StudentService studentService, CityService cityService) {
			super();
			this.studentService = studentService;
			this.cityService = cityService;
		}
		
		@GetMapping("/home")
		public String home() {
			return "index";
		}
		

		@ModelAttribute(name = "cities")
		private List<CityDto> getCities() {
			return cityService.getAll();
		}
		
		@GetMapping("/list")
		public String StudentList(Model model) {
			List<StudentDto> students = studentService.allStudents();
			
			model.addAttribute("students" , students);
			
			return "student-list";
		}

		

		@GetMapping("/add")
		public String addStudent(Model model) {
			StudentDto studentDto = new StudentDto();
			model.addAttribute("studentDto" , studentDto);
			return "student-form";
			
		}

		@PostMapping("/save")
		public String saveStudent(@Valid @ModelAttribute("studentDto") StudentDto studentDto , BindingResult theBindingResult) {
			System.out.println(studentDto);
			
			if(theBindingResult.hasErrors()) {
				return "student-form";
			}else {
				studentService.saveStudent(studentDto);
				return "redirect:/student/list";
			}
		}
		
		@GetMapping("/update")
		public String update(@RequestParam("id") Long id , Model model) {
			StudentDto studentDto = studentService.findById(id);
			
			model.addAttribute("studentDto" , studentDto);
			
			return "student-form";
		}
		
		@GetMapping("/details")
		public String details(@RequestParam("id") Long id , Model model) {
			StudentDto studentDto = studentService.findById(id);
			
			model.addAttribute("studentDto" , studentDto);
			
			return "student-details";
		}
		
		
		@GetMapping("/delete")
		public String delete(@RequestParam("id") Long id) {
			studentService.deleteStudent(id);
			
			return "redirect:/student/list";
		}


}
