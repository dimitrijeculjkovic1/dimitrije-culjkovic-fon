package dimitrije.culjkovic.fon.engineeringproject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dimitrije.culjkovic.fon.engineeringproject.dto.ExamApplicationDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.ExamDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.ProfessorDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.StudentDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.SubjectDto;
import dimitrije.culjkovic.fon.engineeringproject.service.ExamApplicationService;
import dimitrije.culjkovic.fon.engineeringproject.service.ExamService;
import dimitrije.culjkovic.fon.engineeringproject.service.StudentService;
import dimitrije.culjkovic.fon.engineeringproject.validator.ExamApplicationDtoValidator;
import dimitrije.culjkovic.fon.engineeringproject.validator.ExamDtoValidator;

@Controller
@RequestMapping("/application")
public class ExamApplicationController {
	
	private final StudentService studentService;
	
	private final ExamService examService;
	
	private final ExamApplicationService examApplicationService;
	
	
	@Autowired
	public ExamApplicationController(StudentService studentService, ExamService examService,ExamApplicationService examApplicationService) {
		super();
		this.studentService = studentService;
		this.examService = examService;
		this.examApplicationService = examApplicationService;
	}


	@GetMapping("/home")
	public String home() {
		return "index";
	}
	

	@ModelAttribute(name = "students")
	private List<StudentDto> getStudents() {
		return studentService.allStudents();
	}
	
	@ModelAttribute(name = "exams")
	private List<ExamDto> getExams() {
		return examService.allExams();
	}
	
	
	@GetMapping("/list")
	public String ExamApplications(Model model) {
		List<ExamApplicationDto> applications = examApplicationService.allExamApplications();
		System.out.println("+++++++++++++---------+++++++++++++++" + applications);
		model.addAttribute("applications" , applications);
		
		return "application-list";
	}

	

	@GetMapping("/add")
	public String addExamApplication(Model model) {
		
		ExamApplicationDto applicationDto = new ExamApplicationDto();
		model.addAttribute("applicationDto" , applicationDto);
		return "application-form";
		
	}

	@PostMapping("/save")
	public String saveExamApplication(@Valid @ModelAttribute("applicationDto") ExamApplicationDto applicationDto, BindingResult theBindingResult) {
		System.out.println(applicationDto);

		if(theBindingResult.hasErrors()) {
			return "application-form";
		}else {
			examApplicationService.saveExamApplication(applicationDto);
			return "redirect:/application/list";
		}
	}
	
	@GetMapping("/update")
	public String update(@RequestParam("id") Long id , Model model) {
		ExamApplicationDto applicationDto = examApplicationService.findById(id);
		
		model.addAttribute("applicationDto" , applicationDto);
		
		return "application-form";
	}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
		examApplicationService.deleteExam(id);
		
		return "redirect:/application/list";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new ExamApplicationDtoValidator());
	}

}
