package dimitrije.culjkovic.fon.engineeringproject.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import dimitrije.culjkovic.fon.engineeringproject.domain.Student;
import dimitrije.culjkovic.fon.engineeringproject.dto.CityDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.StudentDto;
import dimitrije.culjkovic.fon.engineeringproject.service.CityService;
import dimitrije.culjkovic.fon.engineeringproject.service.StudentService;

@Controller
@RequestMapping("/index")
public class IndexController {
	
	@GetMapping
	public String home() {
		System.out.println("====================================================================");
		System.out.println("====================   IndexController:   ===================");
		System.out.println("====================================================================");
		return "index";
	}
	
	
	

	
	
}
