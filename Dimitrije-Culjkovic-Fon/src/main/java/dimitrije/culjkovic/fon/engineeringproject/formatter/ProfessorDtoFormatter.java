package dimitrije.culjkovic.fon.engineeringproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import dimitrije.culjkovic.fon.engineeringproject.dto.ProfessorDto;
import dimitrije.culjkovic.fon.engineeringproject.service.ProfessorService;

@Component
public class ProfessorDtoFormatter implements Formatter<ProfessorDto> {

	private final ProfessorService professorService;
	
	
	@Autowired
	public ProfessorDtoFormatter(ProfessorService professorService) {
		super();
		this.professorService = professorService;
	}

	@Override
	public String print(ProfessorDto professorDto, Locale locale) {
		
		return professorDto.toString();
	}

	@Override
	public ProfessorDto parse(String prof, Locale locale) throws ParseException {
		
		Long id = Long.parseLong(prof);
		
		ProfessorDto professorDto = professorService.findById(id);
		
		return professorDto;
	}

}
