package dimitrije.culjkovic.fon.engineeringproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import dimitrije.culjkovic.fon.engineeringproject.dto.StudentDto;
import dimitrije.culjkovic.fon.engineeringproject.service.StudentService;

@Component
public class StudentDtoFormatter implements Formatter<StudentDto> {

	private final StudentService studentService;
	
	
	@Autowired
	public StudentDtoFormatter(StudentService studentService) {
		super();
		this.studentService = studentService;
	}

	@Override
	public String print(StudentDto studentDto, Locale locale) {
		
		return studentDto.toString();
	}

	@Override
	public StudentDto parse(String student, Locale locale) throws ParseException {
		Long id = Long.parseLong(student);
		StudentDto studentDto = studentService.findById(id);
		return studentDto;
	}

}
