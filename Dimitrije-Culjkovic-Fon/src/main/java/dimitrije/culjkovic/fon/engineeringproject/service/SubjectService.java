package dimitrije.culjkovic.fon.engineeringproject.service;

import java.util.List;

import dimitrije.culjkovic.fon.engineeringproject.dto.SubjectDto;

public interface SubjectService {
	
	public List<SubjectDto> allSubjects();
	
	public void saveSubject(SubjectDto subjectDto);
	
	public SubjectDto findById(Long id);
	
	public void deleteSubject(Long id);

}
