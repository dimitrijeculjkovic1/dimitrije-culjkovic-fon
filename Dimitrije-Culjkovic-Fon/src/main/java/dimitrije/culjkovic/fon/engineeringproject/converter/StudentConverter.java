package dimitrije.culjkovic.fon.engineeringproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import dimitrije.culjkovic.fon.engineeringproject.domain.City;
import dimitrije.culjkovic.fon.engineeringproject.domain.Student;
import dimitrije.culjkovic.fon.engineeringproject.dto.CityDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.StudentDto;

@Component
public class StudentConverter {
	
	public StudentDto entityToDto(Student student) {
		return new StudentDto(student.getId(), student.getIndexNumber(), student.getFirstName(),
				student.getLastName(), student.getEmail(), student.getAddress(), student.getPhone(),
				student.getCurrentYearofStudy(),
				new CityDto(student.getCity().getId(),student.getCity().getCityNumber(), student.getCity().getCityName()));
	}
	
	public Student dtoToEntity(StudentDto studentDto) {
		return new Student(studentDto.getId(),studentDto.getIndexNumber(),
				studentDto.getFirstName(), studentDto.getLastName(), studentDto.getEmail(), studentDto.getAddress(),
				studentDto.getPhone(), studentDto.getCurrentYearofStudy(),
				new City(studentDto.getCityDto().getId(),studentDto.getCityDto().getCityNumber(), studentDto.getCityDto().getCityName()));
	}
	
	
	public List<Student> dtoToEntityList(List<StudentDto> studentDtoList) {
		List<Student> list = new ArrayList<Student>();
		for(StudentDto studentDto: studentDtoList) {
			list.add(dtoToEntity(studentDto));
		}
		return list;
	}
		
		public List<StudentDto> entityToDtoList(List<Student> list) {
			List<StudentDto> dtolist = new ArrayList<StudentDto>();
			for(Student student: list) {
				dtolist.add(entityToDto(student));
			}
			return dtolist;
		}
	
	
	
	
}
