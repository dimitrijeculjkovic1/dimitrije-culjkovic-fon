package dimitrije.culjkovic.fon.engineeringproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dimitrije.culjkovic.fon.engineeringproject.domain.ExamApplication;
import dimitrije.culjkovic.fon.engineeringproject.dto.ExamApplicationDto;

@Component
public class ExamApplicationConverter {
	
	private final StudentConverter studentConverter;
	
	private final ExamConverter examConverter;

	@Autowired
	public ExamApplicationConverter(StudentConverter studentConverter, ExamConverter examConverter) {
		super();
		this.studentConverter = studentConverter;
		this.examConverter = examConverter;
	}
	
	public ExamApplicationDto entityToDto(ExamApplication examApplication) {
		return new ExamApplicationDto(examApplication.getId(), studentConverter.entityToDto(examApplication.getStudent()),
				examConverter.entityToDto(examApplication.getExam()));
	}
	
	public ExamApplication dtoToEntity(ExamApplicationDto dtoapp) {
		return new ExamApplication(dtoapp.getId(), studentConverter.dtoToEntity(dtoapp.getStudentDto()),
				examConverter.dtoToEntity(dtoapp.getExamDto()));
	}
	
	
	public List<ExamApplication> dtoToEntityList(List<ExamApplicationDto> dtoList) {
		List<ExamApplication> applicatons = new ArrayList<ExamApplication>();
		for (ExamApplicationDto dtoapp : dtoList) {
			applicatons.add(dtoToEntity(dtoapp));
			
		}
		return applicatons;
	}
	
	public List<ExamApplicationDto> entityToDtoList(List<ExamApplication> applications){
		
		List<ExamApplicationDto> applicationsDto = new ArrayList<ExamApplicationDto>();
		
		for(ExamApplication examapp : applications) {
			applicationsDto.add(entityToDto(examapp));
		}
		
		
		return applicationsDto;
	}

}
