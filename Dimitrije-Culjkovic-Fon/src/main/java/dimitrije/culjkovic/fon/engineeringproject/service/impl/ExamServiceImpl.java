package dimitrije.culjkovic.fon.engineeringproject.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dimitrije.culjkovic.fon.engineeringproject.converter.ExamConverter;
import dimitrije.culjkovic.fon.engineeringproject.dao.ExamDAO;
import dimitrije.culjkovic.fon.engineeringproject.domain.Exam;
import dimitrije.culjkovic.fon.engineeringproject.dto.ExamDto;
import dimitrije.culjkovic.fon.engineeringproject.service.ExamService;
@Service
@Transactional
public class ExamServiceImpl implements ExamService{

	private ExamDAO examDAO;
	private ExamConverter examConverter;
	
	
	@Autowired
	public ExamServiceImpl(ExamDAO examDAO, ExamConverter examConverter) {
		super();
		this.examDAO = examDAO;
		this.examConverter = examConverter;
	}

	@Override
	public List<ExamDto> allExams() {
		
		List<Exam> examList = examDAO.findAll();
		List<ExamDto> exams = examConverter.entityToDtoList(examList);
		
		return exams;
	}

	@Override
	public void saveExam(ExamDto examDto) {
		
		
		
		
	examDAO.save(examConverter.dtoToEntity(examDto));
	
	
		
	}

	@Override
	public ExamDto findById(Long id) {
		Optional<Exam> find = examDAO.findById(id);
		return examConverter.entityToDto(find.get());
	}

	@Override
	public void deleteExam(Long id) {
	
		examDAO.deleteById(id);
		
	}


	



}
