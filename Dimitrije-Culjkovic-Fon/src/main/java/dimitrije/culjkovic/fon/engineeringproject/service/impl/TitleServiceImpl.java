package dimitrije.culjkovic.fon.engineeringproject.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dimitrije.culjkovic.fon.engineeringproject.converter.TitleConverter;
import dimitrije.culjkovic.fon.engineeringproject.dao.TitleDAO;
import dimitrije.culjkovic.fon.engineeringproject.domain.Title;
import dimitrije.culjkovic.fon.engineeringproject.dto.TitleDto;
import dimitrije.culjkovic.fon.engineeringproject.service.TitleService;
@Service
@Transactional
public class TitleServiceImpl implements TitleService {
	
	private TitleDAO titleDAO;
	
	private TitleConverter titleConverter;
	
	
	@Autowired
	public TitleServiceImpl(TitleDAO titleDAO, TitleConverter titleConverter) {
		super();
		this.titleDAO = titleDAO;
		this.titleConverter = titleConverter;
	}

	@Override
	public List<TitleDto> getAll() {
	List<Title> titles = titleDAO.findAll();
	List<TitleDto> titlesDto = titleConverter.entityToDtoList(titles);
		return titlesDto;
	}

	@Override
	public TitleDto findById(Long id) {
	Optional<Title> find = titleDAO.findById(id);
		return titleConverter.entityToDto(find.get());
	}

}
