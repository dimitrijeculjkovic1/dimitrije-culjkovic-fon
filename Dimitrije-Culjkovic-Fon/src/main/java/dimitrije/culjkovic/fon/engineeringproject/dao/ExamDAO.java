package dimitrije.culjkovic.fon.engineeringproject.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import dimitrije.culjkovic.fon.engineeringproject.domain.Exam;

@Repository
public interface ExamDAO extends JpaRepository<Exam, Long> {
	
	@Query("select e from Exam e where e.subject.name = ?1")
	public List<Exam> findBySubjectName(String subjectName);

}
