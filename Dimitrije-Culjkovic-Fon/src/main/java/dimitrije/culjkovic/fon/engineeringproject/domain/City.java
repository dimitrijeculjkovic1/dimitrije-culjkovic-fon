package dimitrije.culjkovic.fon.engineeringproject.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "city")
public class City implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1111302042020L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "city_id")
	private Long id;
	@Column(name = "city_number")
	private Long cityNumber;
	@Column(name="city_name")
	private String cityName;
	public City() {

	}
	public City(Long id,Long cityNumber, String cityName) {
		super();
		this.id = id;
		this.cityNumber = cityNumber;
		this.cityName = cityName;
	}
	public Long getCityNumber() {
		return cityNumber;
	}
	public void setCityNumber(Long cityNumber) {
		this.cityNumber = cityNumber;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	@Override
	public String toString() {
		return "City [cityNumber=" + cityNumber + ", cityName=" + cityName + "]";
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	
	
	
	
	
	
	
}
