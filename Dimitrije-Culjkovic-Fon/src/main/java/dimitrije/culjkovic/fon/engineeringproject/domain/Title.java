package dimitrije.culjkovic.fon.engineeringproject.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "title")
public class Title implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 130404042020L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "title_id")
	private Long id;
	@Column(name = "title_number")
	private Long titleNumber;
	@Column(name = "title_name")
	private String titleName;
	public Title() {
	
	}
	public Title(Long id ,Long titleNumber, String titleName) {
		super();
		this.titleNumber = titleNumber;
		this.titleName = titleName;
		this.id = id;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTitleNumber() {
		return titleNumber;
	}
	public void setTitleNumber(Long titleNumber) {
		this.titleNumber = titleNumber;
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	@Override
	public String toString() {
		return "Title [id=" + id + ", titleNumber=" + titleNumber + ", titleName=" + titleName + "]";
	}
	
	
	
	
	
	

}
