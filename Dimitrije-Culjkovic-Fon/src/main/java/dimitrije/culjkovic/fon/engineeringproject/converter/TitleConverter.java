package dimitrije.culjkovic.fon.engineeringproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import dimitrije.culjkovic.fon.engineeringproject.domain.Title;
import dimitrije.culjkovic.fon.engineeringproject.dto.TitleDto;

@Component
public class TitleConverter {
	
	public TitleDto entityToDto(Title title) {
		return new TitleDto(title.getId(), title.getTitleNumber(), title.getTitleName());
	}
	
	public Title dtoToEntity(TitleDto titleDto) {
		return new Title(titleDto.getId(), titleDto.getTitleNumber(), titleDto.getTitleName());
	}
	
	public List<TitleDto> entityToDtoList(List<Title> titles) {
		List<TitleDto> titlesDto = new ArrayList<TitleDto>();
		
		for (Title title : titles) {
			titlesDto.add(entityToDto(title));
		}
		return titlesDto;
	}
	
	public List<Title> DtoToEntityList(List<TitleDto>titlesDto){
		List<Title> titles = new ArrayList<Title>();
		
		for (TitleDto titleDto : titlesDto) {
			titles.add(dtoToEntity(titleDto));
		}
		return titles;
	}

}
