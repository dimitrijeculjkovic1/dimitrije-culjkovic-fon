package dimitrije.culjkovic.fon.engineeringproject.formatter;

import java.text.ParseException;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import dimitrije.culjkovic.fon.engineeringproject.dto.CityDto;
import dimitrije.culjkovic.fon.engineeringproject.service.CityService;

@Component
public class CityDtoFormatter implements Formatter<CityDto>{
	private final CityService cityService;

	@Autowired
	public CityDtoFormatter(CityService cityService) {
		System.out.println("=======CityDtoFormatter: constructor============================================");
		this.cityService = cityService;
		System.out.println(cityService == null);
	}
	
	@Override
	public String print(CityDto cityDto, Locale locale) {
		System.out.println("=======CityDtoFormatter: print============================================");
		System.out.println(cityDto);
		System.out.println("==========================================================================");
		return cityDto.toString();
	}

	@Override
	public CityDto parse(String city, Locale locale) {
		System.out.println("=======CityDtoFormatter: parse============================================");
		System.out.println(city);
		System.out.println("==========================================================================");
		
		Long number=Long.parseLong(city);
		System.out.println(number);
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
     	CityDto cityDto = cityService.findById(number);
		System.out.println("==========================================================================");
		return cityDto;
		
	}
}
