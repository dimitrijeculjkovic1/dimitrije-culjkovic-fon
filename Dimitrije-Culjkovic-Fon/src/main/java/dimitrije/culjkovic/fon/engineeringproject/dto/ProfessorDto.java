package dimitrije.culjkovic.fon.engineeringproject.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.lang.NonNull;

import dimitrije.culjkovic.fon.engineeringproject.domain.City;
import dimitrije.culjkovic.fon.engineeringproject.domain.Title;

public class ProfessorDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 132004042020L;
	
	private Long id;
	@NotBlank
	@Size(min = 3 , message = "Minimal number of characters is 3" )
	private String firstName;
	@NotBlank
	@Size(min = 3 , message = "Minimal number of characters is 3" )
	private String lastName;
	@Email (message = "You must insert valid email address")
	@NotBlank
	private String email;
	@Size(min =3 , message = "Minimal number of characters is 3")
	private String address;
	
	private CityDto cityDto;
	@Size(min = 6 , message = "Minimal number of characters is 6")
	private String phone;
	@NotNull
	private Date reelectionDate;
	@NotNull
	private TitleDto titleDto;

	public ProfessorDto() {

	}

	public ProfessorDto(Long id,String firstName, String lastName, String email, String address, CityDto cityDto, String phone,
			Date reelectionDate, TitleDto titleDto) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.cityDto = cityDto;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.titleDto = titleDto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public CityDto getCityDto() {
		return cityDto;
	}

	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public TitleDto getTitleDto() {
		return titleDto;
	}

	public void setTitleDto(TitleDto titleDto) {
		this.titleDto = titleDto;
	}

	@Override
	public String toString() {
		return "ProfessorDto [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", address=" + address + ", cityDto=" + cityDto + ", phone=" + phone + ", reelectionDate="
				+ reelectionDate + ", titleDto=" + titleDto + "]";
	}
	
	
	
	
	
	

}
