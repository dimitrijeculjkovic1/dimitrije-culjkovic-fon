package dimitrije.culjkovic.fon.engineeringproject.formatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
@Component
public class ReelectionDateFormatter implements Formatter<Date> {

	private SimpleDateFormat simpleDateFormat;
	
	@Autowired
	public ReelectionDateFormatter(SimpleDateFormat simpleDateFormat) {
		
		this.simpleDateFormat = simpleDateFormat;
	}

	@Override
	public String print(Date date, Locale locale) {
		System.out.println(date);
		return date.toString();
	}

	@Override
	public Date parse(String date, Locale locale) throws ParseException {
	
		return simpleDateFormat.parse(date);
	}

}
