package dimitrije.culjkovic.fon.engineeringproject.dto;

import java.io.Serializable;

public class TitleDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 140304042020L;
	
	private Long id;
	
	private Long titleNumber;
	
	private String titleName;

	public TitleDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TitleDto(Long id,Long titleNumber, String titleName) {
		super();
		this.id = id;
		this.titleNumber = titleNumber;
		this.titleName = titleName;
	}
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTitleNumber() {
		return titleNumber;
	}

	public void setTitleNumber(Long titleNumber) {
		this.titleNumber = titleNumber;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	@Override
	public String toString() {
		return "TitleDto [id=" + id + ", titleNumber=" + titleNumber + ", titleName=" + titleName + "]";
	}


	
	

}
