package dimitrije.culjkovic.fon.engineeringproject.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class ExamApplicationDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 193509042020L;
	
	private Long id;
	@NotNull(message = "Choose student")
	private StudentDto studentDto;
	@NotNull(message = "Choose exam")
	private ExamDto examDto;
	
	
	public ExamApplicationDto() {

	}


	public ExamApplicationDto(Long id, StudentDto studentDto, ExamDto examDto) {
		super();
		this.id = id;
		this.studentDto = studentDto;
		this.examDto = examDto;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public StudentDto getStudentDto() {
		return studentDto;
	}


	public void setStudentDto(StudentDto studentDto) {
		this.studentDto = studentDto;
	}


	public ExamDto getExamDto() {
		return examDto;
	}


	public void setExamDto(ExamDto examDto) {
		this.examDto = examDto;
	}


	@Override
	public String toString() {
		return "ExamApplicationDto [id=" + id + ", studentDto=" + studentDto + ", examDto=" + examDto + "]";
	}
	
	
	
	

}
