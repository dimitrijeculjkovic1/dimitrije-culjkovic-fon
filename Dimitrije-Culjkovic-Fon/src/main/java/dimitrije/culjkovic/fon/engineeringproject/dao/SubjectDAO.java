package dimitrije.culjkovic.fon.engineeringproject.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dimitrije.culjkovic.fon.engineeringproject.domain.Subject;
@Repository
public interface SubjectDAO extends JpaRepository<Subject, Long> {

}
