package dimitrije.culjkovic.fon.engineeringproject.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dimitrije.culjkovic.fon.engineeringproject.converter.ProfessorConverter;
import dimitrije.culjkovic.fon.engineeringproject.dao.ProfessorDao;
import dimitrije.culjkovic.fon.engineeringproject.domain.Professor;
import dimitrije.culjkovic.fon.engineeringproject.dto.ProfessorDto;
import dimitrije.culjkovic.fon.engineeringproject.service.ProfessorService;
@Service
@Transactional
public class ProfessorServiceImpl implements ProfessorService {

	private ProfessorDao professorDAO;
	private ProfessorConverter professorConverter;
	
	
	@Autowired
	public ProfessorServiceImpl(ProfessorDao professorDAO, ProfessorConverter professorConverter) {
		super();
		this.professorDAO = professorDAO;
		this.professorConverter = professorConverter;
	}

	@Override
	public List<ProfessorDto> getProfessors() {
	
		List<Professor> professors = professorDAO.findAll();
		List<ProfessorDto> dtoProfessors = professorConverter.entityToDtoList(professors);
		
		return dtoProfessors;
	}

	@Override
	public void saveProfessor(ProfessorDto professorDto) {
	
		professorDAO.save(professorConverter.DtoToEntity(professorDto));
		
	}

	@Override
	public ProfessorDto findById(Long id) {
		Optional<Professor> find = professorDAO.findById(id);
		return professorConverter.entityToDto(find.get());
	}

	@Override
	public void delete(Long id) {
		professorDAO.deleteById(id);
		
	}

}
