package dimitrije.culjkovic.fon.engineeringproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import dimitrije.culjkovic.fon.engineeringproject.dto.ExamDto;
import dimitrije.culjkovic.fon.engineeringproject.service.ExamService;

@Component
public class ExamDtoFormatter implements Formatter<ExamDto> {

	private final ExamService examService;
	
	
	@Autowired
	public ExamDtoFormatter(ExamService examService) {
		super();
		this.examService = examService;
	}

	@Override
	public String print(ExamDto examDto, Locale locale) {
		System.out.println(examDto);
		return examDto.toString();
	}

	@Override
	public ExamDto parse(String exam, Locale locale) throws ParseException {
		Long id = Long.parseLong(exam);
		ExamDto examDto = examService.findById(id);
		return examDto;
	}

}
