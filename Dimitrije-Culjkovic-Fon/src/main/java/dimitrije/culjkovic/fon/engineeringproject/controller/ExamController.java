package dimitrije.culjkovic.fon.engineeringproject.controller;


import java.text.SimpleDateFormat;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import dimitrije.culjkovic.fon.engineeringproject.dto.ExamDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.ProfessorDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.SubjectDto;
import dimitrije.culjkovic.fon.engineeringproject.service.ExamService;
import dimitrije.culjkovic.fon.engineeringproject.service.ProfessorService;
import dimitrije.culjkovic.fon.engineeringproject.service.SubjectService;
import dimitrije.culjkovic.fon.engineeringproject.validator.ExamDtoValidator;

@Controller
@RequestMapping("/exam")
public class ExamController {
	private final ExamService examService;
	private final SubjectService subjectService;
	private final ProfessorService professorService;
	private final SimpleDateFormat simpleDateFormat;
	

	public ExamController(ExamService examService, SubjectService subjectService, ProfessorService professorService,
			SimpleDateFormat simpleDateFormat) {
		super();
		this.examService = examService;
		this.subjectService = subjectService;
		this.professorService = professorService;
		this.simpleDateFormat = simpleDateFormat;
		
	}
	
	@GetMapping("/home")
	public String home() {
		return "index";
	}
	

	@ModelAttribute(name = "subjects")
	private List<SubjectDto> getSubjects() {
		return subjectService.allSubjects();
	}
	
	@ModelAttribute(name = "professors")
	private List<ProfessorDto> getProfessors() {
		return professorService.getProfessors();
	}
	
	
	@GetMapping("/list")
	public String ExamList(Model model) {
		List<ExamDto> exams = examService.allExams();
		
		model.addAttribute("exams" , exams);
		
		return "exam-list";
	}

	

	@GetMapping("/add")
	public String addExam(Model model) {
		ExamDto examDto = new ExamDto();
		model.addAttribute("examDto" , examDto);
		return "exam-form";
		
	}

	@PostMapping("/save")
	public String saveExam(@Valid @ModelAttribute("examDto") ExamDto examDto , BindingResult theBindingResult) {
		System.out.println(examDto);
        
		if(theBindingResult.hasErrors()) {
			return "exam-form";
		}else {
			examService.saveExam(examDto);
			return "redirect:/exam/list";
		}
	}
	
	@GetMapping("/update")
	public String update(@RequestParam("id") Long id , Model model) {
		ExamDto examDto = examService.findById(id);
		
		model.addAttribute("examDto" , examDto);
		
		return "exam-form";
	}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
		examService.deleteExam(id);
		
		return "redirect:/exam/list";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new ExamDtoValidator(examService, simpleDateFormat));
	}

}
