package dimitrije.culjkovic.fon.engineeringproject.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import dimitrije.culjkovic.fon.engineeringproject.enumeration.Semester;

public class SubjectDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 175407042020L;
	private Long id;
	@NotBlank
	@Size(min = 3 , message = "Minimal number of characters is 3")
	private String name;
	
	private String description;
	
	private int yearOfStudy;
	
	private Semester semester;

	public SubjectDto() {
	
	}

	public SubjectDto(Long id, String name, String description, int yearOfStudy, Semester semester) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public Semester getSemester() {
		return semester;
	}

	public void setSemester(Semester semester) {
		this.semester = semester;
	}

	@Override
	public String toString() {
		return "SubjectDto [id=" + id + ", name=" + name + ", description=" + description + ", yearOfStudy="
				+ yearOfStudy + ", semester=" + semester + "]";
	}
	
	

	
	
	
}
