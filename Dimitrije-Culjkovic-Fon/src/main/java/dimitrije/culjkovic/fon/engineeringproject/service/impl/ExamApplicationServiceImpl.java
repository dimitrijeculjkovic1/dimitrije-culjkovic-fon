package dimitrije.culjkovic.fon.engineeringproject.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dimitrije.culjkovic.fon.engineeringproject.converter.ExamApplicationConverter;
import dimitrije.culjkovic.fon.engineeringproject.dao.ExamApplicationDAO;
import dimitrije.culjkovic.fon.engineeringproject.domain.ExamApplication;
import dimitrije.culjkovic.fon.engineeringproject.dto.ExamApplicationDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.ExamDto;
import dimitrije.culjkovic.fon.engineeringproject.service.ExamApplicationService;

@Service
@Transactional
public class ExamApplicationServiceImpl implements ExamApplicationService {

	private ExamApplicationDAO examApplicationDAO;
	
	private ExamApplicationConverter examApplicationConvertor;
	
	
	@Autowired
	public ExamApplicationServiceImpl(ExamApplicationDAO examApplicationDAO,
			ExamApplicationConverter examApplicationConvertor) {
		super();
		this.examApplicationDAO = examApplicationDAO;
		this.examApplicationConvertor = examApplicationConvertor;
	}

	@Override
	public List<ExamApplicationDto> allExamApplications() {
		
		List<ExamApplication> applications = examApplicationDAO.findAll();
		
		List<ExamApplicationDto> applicationsDto = examApplicationConvertor.entityToDtoList(applications);
		
		return applicationsDto;
	}

	@Override
	public void saveExamApplication(ExamApplicationDto examApplicationDto) {
		examApplicationDAO.save(examApplicationConvertor.dtoToEntity(examApplicationDto));
		
	}

	@Override
	public ExamApplicationDto findById(Long id) {
	Optional<ExamApplication> find = examApplicationDAO.findById(id);
		return examApplicationConvertor.entityToDto(find.get());
	}

	@Override
	public void deleteExam(Long id) {
		examApplicationDAO.deleteById(id);
		
	}

	@Override
	public List<ExamDto> findExams(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		
		
		return null;
	}

}
