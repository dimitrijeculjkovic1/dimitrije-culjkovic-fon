package dimitrije.culjkovic.fon.engineeringproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import dimitrije.culjkovic.fon.engineeringproject.dto.TitleDto;
import dimitrije.culjkovic.fon.engineeringproject.service.TitleService;

@Component
public class TitleDtoFormatter implements Formatter<TitleDto> {
	
	private final TitleService titleService;

	@Autowired
	public TitleDtoFormatter(TitleService titleService) {
		super();
		this.titleService = titleService;
	}

	@Override
	public String print(TitleDto titleDto, Locale locale) {
		
		System.out.println(titleDto);
		
		return titleDto.toString();
	}

	@Override
	public TitleDto parse(String title, Locale locale) throws ParseException {
	
		Long number = Long.parseLong(title);
		System.out.println(number);
		TitleDto titleDto = titleService.findById(number);
		return titleDto;
	}
	
	

}
