package dimitrije.culjkovic.fon.engineeringproject.service;

import java.util.List;

import dimitrije.culjkovic.fon.engineeringproject.dto.TitleDto;

public interface TitleService {

	List<TitleDto> getAll();
	
	TitleDto findById(Long id);
}
