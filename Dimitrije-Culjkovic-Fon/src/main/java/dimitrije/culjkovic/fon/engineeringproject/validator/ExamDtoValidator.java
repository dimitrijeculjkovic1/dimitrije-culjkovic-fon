package dimitrije.culjkovic.fon.engineeringproject.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import dimitrije.culjkovic.fon.engineeringproject.dto.ExamDto;
import dimitrije.culjkovic.fon.engineeringproject.service.ExamService;

public class ExamDtoValidator implements Validator {
	
	private final ExamService examService;
	
	private final SimpleDateFormat simpleDateFormat;
	
	

	@Autowired
	public ExamDtoValidator(ExamService examService, SimpleDateFormat simpleDateFormat) {
		super();
		this.examService = examService;
		this.simpleDateFormat = simpleDateFormat;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		
		return ExamDto.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		
	ExamDto examDto = (ExamDto) object;	
	
	Date date = new Date();
	
	Date formattedDate;
	
	try {
		formattedDate = simpleDateFormat.parse((simpleDateFormat.format(date)));
		Date tomorrow = new Date(formattedDate.getTime() + (1000 * 60 * 60 * 24));
		System.out.println(formattedDate);
		System.out.println("++++++++++++++++"+tomorrow);
		
		if(examDto.getExamDate().before(tomorrow)) {
			errors.rejectValue("examDate", "ExamDto.class", "Invalid date!");
		}
	} catch(ParseException e) {
		e.printStackTrace();
	}
	
	if(examDto.getExamDate() == null) {
		errors.rejectValue("examDate", "ExamDto.class", "Must choose date after: " + date.toString());
	}

	}
	
	
	
	

}
