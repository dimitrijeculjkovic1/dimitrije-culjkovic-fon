package dimitrije.culjkovic.fon.engineeringproject.service;

import java.util.List;

import dimitrije.culjkovic.fon.engineeringproject.dto.CityDto;

public interface CityService {
   
	List<CityDto> getAll();
	
	CityDto findById(Long id);
}
