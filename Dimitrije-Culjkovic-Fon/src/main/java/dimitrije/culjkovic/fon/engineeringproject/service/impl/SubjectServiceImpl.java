package dimitrije.culjkovic.fon.engineeringproject.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dimitrije.culjkovic.fon.engineeringproject.converter.SubjectConverter;
import dimitrije.culjkovic.fon.engineeringproject.dao.SubjectDAO;
import dimitrije.culjkovic.fon.engineeringproject.domain.Subject;
import dimitrije.culjkovic.fon.engineeringproject.dto.SubjectDto;
import dimitrije.culjkovic.fon.engineeringproject.service.SubjectService;

@Service
@Transactional
public class SubjectServiceImpl implements SubjectService {

	private SubjectDAO subjectDAO;
	private SubjectConverter subjectConverter;
	
	
	
	@Autowired
	public SubjectServiceImpl(SubjectDAO subjectDAO, SubjectConverter subjectConverter) {
		super();
		this.subjectDAO = subjectDAO;
		this.subjectConverter = subjectConverter;
	}

	@Override
	public List<SubjectDto> allSubjects() {
	
		List<Subject> list = subjectDAO.findAll();
		List<SubjectDto> subjects = subjectConverter.entityToDtoList(list);
		
		return subjects;
	}

	@Override
	public void saveSubject(SubjectDto subjectDto) {
		subjectDAO.save(subjectConverter.dtoToEntity(subjectDto));
		
	}

	@Override
	public SubjectDto findById(Long id) {
		Optional<Subject> find = subjectDAO.findById(id);
		return subjectConverter.entityToDto(find.get());
	}

	@Override
	public void deleteSubject(Long id) {
	subjectDAO.deleteById(id);
		
	}

}
