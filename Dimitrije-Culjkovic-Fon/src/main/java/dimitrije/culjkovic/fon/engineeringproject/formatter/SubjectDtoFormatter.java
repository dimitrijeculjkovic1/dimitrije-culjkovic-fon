package dimitrije.culjkovic.fon.engineeringproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import dimitrije.culjkovic.fon.engineeringproject.dto.SubjectDto;
import dimitrije.culjkovic.fon.engineeringproject.service.SubjectService;

@Component
public class SubjectDtoFormatter implements Formatter<SubjectDto> {

	private final SubjectService subjectService;
	
	
	
	public SubjectDtoFormatter(SubjectService subjectService) {
		super();
		this.subjectService = subjectService;
		System.out.println("+++++++++++++");
		System.out.println(subjectService == null);
	}

	@Override
	public String print(SubjectDto subjectDto, Locale locale) {
		System.out.println(subjectDto);
		return subjectDto.toString();
	}

	@Override
	public SubjectDto parse(String subject, Locale locale) throws ParseException {
		Long number = Long.parseLong(subject);
		System.out.println(number);
		SubjectDto subjectDto = subjectService.findById(number);
		return subjectDto;
	}

}
