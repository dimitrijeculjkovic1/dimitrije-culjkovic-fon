package dimitrije.culjkovic.fon.engineeringproject.service;

import java.util.List;

import dimitrije.culjkovic.fon.engineeringproject.dto.ExamDto;

public interface ExamService {
	
	public List<ExamDto> allExams();
	
	public void saveExam(ExamDto examDto);
	
	public ExamDto findById(Long id);
	
	public void deleteExam(Long id);
	
	

}
