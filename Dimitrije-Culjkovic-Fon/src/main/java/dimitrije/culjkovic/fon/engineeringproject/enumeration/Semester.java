package dimitrije.culjkovic.fon.engineeringproject.enumeration;

public enum Semester {
	
	SUMMER("Summer") , WINTER("Winter");
	
	private String semester;

	private Semester(String semester) {
		this.semester = semester;
	}

	public String getSemester() {
		return semester;
	}
	
	

}
