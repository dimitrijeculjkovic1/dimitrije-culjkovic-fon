package dimitrije.culjkovic.fon.engineeringproject.service;

import java.util.Date;
import java.util.List;

import dimitrije.culjkovic.fon.engineeringproject.dto.ExamApplicationDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.ExamDto;


public interface ExamApplicationService {
	
	public List<ExamApplicationDto> allExamApplications();
	
	public void saveExamApplication(ExamApplicationDto examApplicationDto);
	
	public ExamApplicationDto findById(Long id);
	
	public void deleteExam(Long id);
	
	public List<ExamDto> findExams(Date date);

}
