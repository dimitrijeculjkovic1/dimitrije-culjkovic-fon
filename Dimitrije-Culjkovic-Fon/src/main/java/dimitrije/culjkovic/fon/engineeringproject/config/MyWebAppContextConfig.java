package dimitrije.culjkovic.fon.engineeringproject.config;


import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import dimitrije.culjkovic.fon.engineeringproject.formatter.CityDtoFormatter;
import dimitrije.culjkovic.fon.engineeringproject.formatter.ExamApplicationDtoFormatter;
import dimitrije.culjkovic.fon.engineeringproject.formatter.ExamDtoFormatter;
import dimitrije.culjkovic.fon.engineeringproject.formatter.ProfessorDtoFormatter;
import dimitrije.culjkovic.fon.engineeringproject.formatter.ReelectionDateFormatter;
import dimitrije.culjkovic.fon.engineeringproject.formatter.SemesterFormatter;
import dimitrije.culjkovic.fon.engineeringproject.formatter.StudentDtoFormatter;
import dimitrije.culjkovic.fon.engineeringproject.formatter.SubjectDtoFormatter;
import dimitrije.culjkovic.fon.engineeringproject.formatter.TitleDtoFormatter;
import dimitrije.culjkovic.fon.engineeringproject.service.CityService;
import dimitrije.culjkovic.fon.engineeringproject.service.ExamApplicationService;
import dimitrije.culjkovic.fon.engineeringproject.service.ExamService;
import dimitrije.culjkovic.fon.engineeringproject.service.ProfessorService;
import dimitrije.culjkovic.fon.engineeringproject.service.StudentService;
import dimitrije.culjkovic.fon.engineeringproject.service.SubjectService;
import dimitrije.culjkovic.fon.engineeringproject.service.TitleService;


@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {
		"dimitrije.culjkovic.fon.engineeringproject.controller"
		})
	
public class MyWebAppContextConfig implements WebMvcConfigurer {
	
	private CityService cityService;
	private TitleService titleService;
	private SubjectService subjectService;
	private ProfessorService professorService;
	private StudentService studentService;
	private ExamService examService;
	private ExamApplicationService examApplicationService;
	
	@Autowired
	public MyWebAppContextConfig(CityService cityService, TitleService titleService,SubjectService subjectService,ProfessorService professorService,
				StudentService studentService , ExamService examService , ExamApplicationService examApplicationService) {
		this.cityService = cityService;
		this.titleService= titleService;
		this.subjectService = subjectService;
		this.professorService = professorService;
		this.studentService = studentService;
		this.examService = examService;
		this.examApplicationService = examApplicationService;
		System.out.println("=================================================================");
		System.out.println("==================== MyWebContextConfig =========================");
		System.out.println("=================================================================");
	}
	

	
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/view/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("index");
	}
	
	@Bean(name = "simpleDateFormat")
	public SimpleDateFormat simpleDateFormat() {
		return new SimpleDateFormat("yyyy-MM-dd");
	}
	
	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatter(new CityDtoFormatter(cityService));
		registry.addFormatter(new SemesterFormatter());
		registry.addFormatter(new ReelectionDateFormatter(simpleDateFormat()));
		registry.addFormatter(new TitleDtoFormatter(titleService));
		registry.addFormatter(new SubjectDtoFormatter(subjectService));
		registry.addFormatter(new ProfessorDtoFormatter(professorService) );
		registry.addFormatter(new StudentDtoFormatter(studentService));
		registry.addFormatter(new ExamDtoFormatter(examService));
		registry.addFormatter(new ExamApplicationDtoFormatter(examApplicationService));
	}
	

	

}
