package dimitrije.culjkovic.fon.engineeringproject.dto;

import java.io.Serializable;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import dimitrije.culjkovic.fon.engineeringproject.domain.City;

public class StudentDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 173103042020L;
	private Long id;
	@NotNull
	@Size(min = 10 , message="Minimal number of characters is 10")
	private String indexNumber;
	@NotNull
	@Size(min = 3 , message = "Minimal number of characters is 3")
	private String firstName;
	@NotNull
	@Size(min = 3 , message = "Minimal number of characters is 3")
	private String lastName;
	@Email(message = "email should be valid")
	@NotBlank
	private String email;
	@Size(min = 3 , message = "Minimal number of characters is 3")
	private String address;
	private CityDto cityDto;
	@Size(min = 6 , message = "Minimal number of characters is 6")
	private String phone;
	@NotNull
	private int currentYearofStudy;
	public StudentDto() {
	}
	public StudentDto(Long id,String indexNumber, String firstName, String lastName, String email, String address,
			String phone, int currentYearofStudy, CityDto cityDto) {
		super();
		this.id = id;
		this.indexNumber = indexNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.cityDto = cityDto;
		this.phone = phone;
		this.currentYearofStudy = currentYearofStudy;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIndexNumber() {
		return indexNumber;
	}
	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public CityDto getCityDto() {
		return cityDto;
	}
	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getCurrentYearofStudy() {
		return currentYearofStudy;
	}
	public void setCurrentYearofStudy(int currentYearofStudy) {
		this.currentYearofStudy = currentYearofStudy;
	}
	@Override
	public String toString() {
		return "StudentDto [id=" + id + ", indexNumber=" + indexNumber + ", firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + ", address=" + address + ", city=" + cityDto + ", phone=" + phone
				+ ", currentYearofStudy=" + currentYearofStudy + "]";
	}
	
	
	
	
	
}
