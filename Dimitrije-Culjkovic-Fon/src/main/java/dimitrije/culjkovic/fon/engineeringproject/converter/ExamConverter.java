package dimitrije.culjkovic.fon.engineeringproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dimitrije.culjkovic.fon.engineeringproject.domain.Exam;
import dimitrije.culjkovic.fon.engineeringproject.dto.ExamDto;

@Component
public class ExamConverter {
	
	private final ProfessorConverter professorConverter;
	
	private final SubjectConverter subjectConverter;
	
	
	@Autowired
	public ExamConverter(ProfessorConverter professorConverter , SubjectConverter subjectConverter) {
		this.professorConverter = professorConverter;
		this.subjectConverter = subjectConverter;
	}



	public ExamDto entityToDto(Exam exam) {
		return new ExamDto(exam.getId(), professorConverter.entityToDto(exam.getProfessor()) , subjectConverter.entityToDto(exam.getSubject()), exam.getExamDate());
		
	}
	
	public Exam dtoToEntity(ExamDto examDto) {
		
		return new Exam(examDto.getId(), professorConverter.DtoToEntity(examDto.getProfessorDto()),
				subjectConverter.dtoToEntity(examDto.getSubjectDto()), examDto.getExamDate());
		
	}
	
	public List<Exam> dtoToEntityList(List<ExamDto> dtoExams){
		List<Exam> exams = new ArrayList<Exam>();
		
		for (ExamDto examDto : dtoExams) {
			exams.add(dtoToEntity(examDto));
		}
		return exams;
		
	}
	
	public List<ExamDto> entityToDtoList(List<Exam> exams){
		List<ExamDto> dtoExams = new ArrayList<ExamDto>();
		for (Exam exam : exams) {
		dtoExams.add(entityToDto(exam));	
		}
		return dtoExams;
	}
	

}
