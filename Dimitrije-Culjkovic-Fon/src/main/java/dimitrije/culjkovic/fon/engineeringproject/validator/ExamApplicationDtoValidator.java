package dimitrije.culjkovic.fon.engineeringproject.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import dimitrije.culjkovic.fon.engineeringproject.dto.ExamApplicationDto;
import dimitrije.culjkovic.fon.engineeringproject.service.ExamApplicationService;
import dimitrije.culjkovic.fon.engineeringproject.service.ExamService;

public class ExamApplicationDtoValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		
		return ExamApplicationDto.class.equals(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		ExamApplicationDto examApplicationDto = (ExamApplicationDto)object;
	
		if(examApplicationDto.getStudentDto().getCurrentYearofStudy() < examApplicationDto.getExamDto().getSubjectDto().getYearOfStudy() ) {
			errors.rejectValue("studentDto", "ExamApplicationDto.studentDto","Student year of study is lower than selected subject");
		}
		
	}

}
