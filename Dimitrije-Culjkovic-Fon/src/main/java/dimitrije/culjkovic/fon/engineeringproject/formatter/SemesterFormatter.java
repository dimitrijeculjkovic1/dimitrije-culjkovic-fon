package dimitrije.culjkovic.fon.engineeringproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;

import dimitrije.culjkovic.fon.engineeringproject.enumeration.Semester;

public class SemesterFormatter implements Formatter<Semester> {

	@Override
	public String print(Semester semester, Locale locale) {
		System.out.println(semester);
	
		return semester.toString();
	}

	@Override
	public Semester parse(String semester, Locale locale) throws ParseException {
		System.out.println(semester);
		
		for (Semester sem : Semester.values()) {
			
			if(sem.getSemester().equals(semester)) {
				return sem;
			}
			
		}
		return null;
	}

}
