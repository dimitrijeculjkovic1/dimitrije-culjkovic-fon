package dimitrije.culjkovic.fon.engineeringproject.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dimitrije.culjkovic.fon.engineeringproject.converter.CityConverter;
import dimitrije.culjkovic.fon.engineeringproject.dao.CityDAO;
import dimitrije.culjkovic.fon.engineeringproject.domain.City;
import dimitrije.culjkovic.fon.engineeringproject.dto.CityDto;
import dimitrije.culjkovic.fon.engineeringproject.service.CityService;

@Service
@Transactional
public class CityServiceImpl implements CityService {
	
	private CityDAO cityDAO;
	private CityConverter cityConverter;
	
	
	@Autowired
	public CityServiceImpl(CityDAO cityDAO, CityConverter cityConverter) {
		
		this.cityDAO = cityDAO;
		this.cityConverter = cityConverter;
	}

	@Override
	public List<CityDto> getAll() {
	
		List<City> list = cityDAO.findAll();
		List<CityDto> cities = cityConverter.entityToDtoList(list);
		
		return cities;
	}

	@Override
	public CityDto findById(Long id) {
	Optional<City> find = cityDAO.findById(id);
	
		return cityConverter.entityToDto(find.get());
	}
	
	
	

}
