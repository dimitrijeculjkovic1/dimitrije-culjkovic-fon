package dimitrije.culjkovic.fon.engineeringproject.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import dimitrije.culjkovic.fon.engineeringproject.dto.ProfessorDto;
import dimitrije.culjkovic.fon.engineeringproject.dto.SubjectDto;
import dimitrije.culjkovic.fon.engineeringproject.enumeration.Semester;
import dimitrije.culjkovic.fon.engineeringproject.service.SubjectService;

@Controller
@RequestMapping("/subject")
public class SubjectController {
	
	
	//potrebno je ubaciti studentservice
		private final SubjectService subjectService;
		

		
		@Autowired
		public SubjectController(SubjectService subjectService) {
			super();
			this.subjectService = subjectService;
		}


		@GetMapping("/home")
		public String home() {
			return "index";
		}

		@GetMapping("/list")
		public String SubjectList(Model model) {
			List<SubjectDto> subjects = subjectService.allSubjects();
			
			model.addAttribute("subjects" , subjects);
			
			return "subject-list";
		}
		
		
		@ModelAttribute(name = "semesters")
		private List<Semester> getSemesters() {
	
			return new ArrayList<Semester>(Arrays.asList(Semester.values()));
		}

		

		@GetMapping("/add")
		public String addSubject(Model model) {
			SubjectDto subjectDto = new SubjectDto();
			model.addAttribute("subjectDto" , subjectDto);
			return "subject-form";
			
		}

		@PostMapping("/save")
		public String saveSubject(@Valid @ModelAttribute("subjectDto") SubjectDto subjectDto , BindingResult theBindingResult) {
			System.out.println(subjectDto);
			
			if(theBindingResult.hasErrors()) {
				return "subject-form";
			}else {
				
				subjectService.saveSubject(subjectDto);
				return "redirect:/subject/list";
			}
		}
		
		@GetMapping("/update")
		public String update(@RequestParam("id") Long id , Model model) {
			SubjectDto subjectDto = subjectService.findById(id);
			
			model.addAttribute("subjectDto" , subjectDto);
			
			return "subject-form";
		}
		
		@GetMapping("/details")
		public String details(@RequestParam("id") Long id , Model model) {
			SubjectDto subjectDto = subjectService.findById(id);
			
			model.addAttribute("subjectDto" , subjectDto);
			
			return "subject-details";
		}
		
		
		@GetMapping("/delete")
		public String delete(@RequestParam("id") Long id) {
			subjectService.deleteSubject(id);
			
			return "redirect:/subject/list";
		}

}
