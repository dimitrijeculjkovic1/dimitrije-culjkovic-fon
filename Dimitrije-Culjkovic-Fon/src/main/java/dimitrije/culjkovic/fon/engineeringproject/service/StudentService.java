package dimitrije.culjkovic.fon.engineeringproject.service;

import java.util.List;



import dimitrije.culjkovic.fon.engineeringproject.domain.Student;
import dimitrije.culjkovic.fon.engineeringproject.dto.StudentDto;

public interface StudentService {
	
	public List<StudentDto> allStudents();
	
	public void saveStudent(StudentDto studentDto);
	
	public StudentDto findById(Long id);
	
	public void deleteStudent(Long id);
		

}
