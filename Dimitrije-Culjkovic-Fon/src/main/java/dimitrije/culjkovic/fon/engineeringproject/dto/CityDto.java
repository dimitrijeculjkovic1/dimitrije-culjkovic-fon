package dimitrije.culjkovic.fon.engineeringproject.dto;

public class CityDto {
	private Long id;
	private Long cityNumber;
	private String cityName;
	public CityDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CityDto(Long id, Long cityNumber, String cityName) {
		super();
		this.id = id;
		this.cityNumber = cityNumber;
		this.cityName = cityName;
	}

	public Long getCityNumber() {
		return cityNumber;
	}
	public void setCityNumber(Long cityNumber) {
		this.cityNumber = cityNumber;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	@Override
	public String toString() {
		return "CityDto [cityNumber=" + cityNumber + ", cityName=" + cityName + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

}
