package dimitrije.culjkovic.fon.engineeringproject.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import dimitrije.culjkovic.fon.engineeringproject.domain.City;
import dimitrije.culjkovic.fon.engineeringproject.dto.CityDto;

@Component
public class CityConverter {
	
	public CityDto entityToDto(City city) {
		return new CityDto(city.getId(), city.getCityNumber(), city.getCityName());
	}
	
	public City dtoToEntity(CityDto cityDto) {
		return new City(cityDto.getId(),cityDto.getCityNumber(),cityDto.getCityName());
	}
	
	
	public List<CityDto> entityToDtoList(List<City> cityList) {
		List<CityDto> cityDtoList = new ArrayList<CityDto>();
		for(City city: cityList) {
			cityDtoList.add(entityToDto(city));
		}
		return cityDtoList;
	}
	
	public List<City> dtoToEntityList(List<CityDto> cityDtoList) {
		List<City> entityList = new ArrayList<City>();
		for(CityDto cityDto: cityDtoList) {
			entityList.add(dtoToEntity(cityDto));
		}
		return entityList;
	}

}
