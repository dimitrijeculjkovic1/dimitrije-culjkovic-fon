package dimitrije.culjkovic.fon.engineeringproject.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import dimitrije.culjkovic.fon.engineeringproject.enumeration.Semester;

@Entity
@Table(name = "subject")
public class Subject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 110405042020L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "subject_id")
	private Long id;
	@Column(name = "subject_name")
	private String name;
	@Column(name = "description")
	private String description;
	@Column (name = "year_of_study")
	private int yearOfStudy;
	@Column (name = "semester")
	private Semester semester;
	
	
	public Subject() {
	
	}


	public Subject(Long id,String name, String description, int yearOfStudy, Semester semester) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public int getYearOfStudy() {
		return yearOfStudy;
	}


	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}


	public Semester getSemester() {
		return semester;
	}


	public void setSemester(Semester semester) {
		this.semester = semester;
	}


	@Override
	public String toString() {
		return "Subject [id=" + id + ", name=" + name + ", description=" + description + ", yearOfStudy=" + yearOfStudy
				+ ", semester=" + semester + "]";
	}
	
	
	
	

}
