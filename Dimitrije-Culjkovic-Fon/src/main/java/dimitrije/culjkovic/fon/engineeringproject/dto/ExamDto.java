package dimitrije.culjkovic.fon.engineeringproject.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class ExamDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 151508042020L;

	private Long id;
	
	@NotNull
	private ProfessorDto professorDto;
	
	@NotNull
	private SubjectDto subjectDto;

	@NotNull
	private Date examDate;

	public ExamDto() {

	}

	public ExamDto(Long id, ProfessorDto professorDto, SubjectDto subjectDto, Date examDate) {
		super();
		this.id = id;
		this.professorDto = professorDto;
		this.subjectDto = subjectDto;
		this.examDate = examDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProfessorDto getProfessorDto() {
		return professorDto;
	}

	public void setProfessorDto(ProfessorDto professorDto) {
		this.professorDto = professorDto;
	}

	public SubjectDto getSubjectDto() {
		return subjectDto;
	}

	public void setSubjectDto(SubjectDto subjectDto) {
		this.subjectDto = subjectDto;
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

	@Override
	public String toString() {
		return "ExamDto [id=" + id + ", professorDto=" + professorDto + ", subjectDto=" + subjectDto + ", examDate="
				+ examDate + "]";
	}
	
	
	
	
}
