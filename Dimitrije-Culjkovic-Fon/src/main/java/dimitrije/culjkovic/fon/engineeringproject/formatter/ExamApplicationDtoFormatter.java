package dimitrije.culjkovic.fon.engineeringproject.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import dimitrije.culjkovic.fon.engineeringproject.dto.ExamApplicationDto;
import dimitrije.culjkovic.fon.engineeringproject.service.ExamApplicationService;

public class ExamApplicationDtoFormatter implements Formatter<ExamApplicationDto> {

	private final ExamApplicationService examApplicationService;
	
	
	@Autowired
	public ExamApplicationDtoFormatter(ExamApplicationService examApplicationService) {
		super();
		this.examApplicationService = examApplicationService;
	}

	@Override
	public String print(ExamApplicationDto object, Locale locale) {
		System.out.println(object);
		return object.toString();
	}

	@Override
	public ExamApplicationDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		ExamApplicationDto examApplicationDto = examApplicationService.findById(id);
		return examApplicationDto;
	}

}
