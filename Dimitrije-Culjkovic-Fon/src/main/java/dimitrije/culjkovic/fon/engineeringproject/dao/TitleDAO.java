package dimitrije.culjkovic.fon.engineeringproject.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dimitrije.culjkovic.fon.engineeringproject.domain.Title;
@Repository
public interface TitleDAO extends JpaRepository<Title, Long> {

}
