<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<title>Pocetna</title>
</head>
<body>
<%@ include file='home.jsp' %>

<br> <br>
<center><h2>STUDENT SERVICE</h2> </center>

<br> <br>

<c:url var="addStudent" value="/student/add"></c:url>
<c:url var="studentList" value="/student/list"></c:url>
<c:url var="addProfessor" value="/professor/add"></c:url>
<c:url var="professorList" value="/professor/list"></c:url>
<c:url var="addSubject" value="/subject/add"></c:url>
<c:url var="subjectList" value="/subject/list"></c:url>
<c:url var="addExam" value="/exam/add"></c:url>
<c:url var="examList" value="/exam/list"></c:url>
<c:url var="addApplication" value="/application/add"></c:url>
<c:url var="applications" value="/application/list"></c:url>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">STUDENTS</th>
      <th scope="col">PROFESSORS</th>
      <th scope="col">SUBJECT</th>
      <th scope="col">EXAM</th>
      
      <th scope="col">APPLICATION</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="${addStudent}">ADD STUDENT</a></td>
      <td><a href="${addProfessor}">ADD PROFESSOR</a></td>
      <td><a href="${addSubject}">ADD SUBJECT</a></td>
      <td><a href="${addExam}">ADD EXAM</a></td>
      <td><a href="${addApplication}">ADD APPLICATION</a></td>            
    </tr>
    <tr>
      <td><a href="${studentList}">ALL STUDENTS</a></td>
      <td><a href="${professorList}">ALL PROFESSORS</a></td>
      <td><a href="${subjectList}">ALL SUBJECTS</a></td>
      <td><a href="${examList}">ALL EXAMS</a></td>
      <td><a href="${applications}">ALL APPLICATIONS</a></td>
    </tr>
    
  </tbody>
</table>

</body>
</html>