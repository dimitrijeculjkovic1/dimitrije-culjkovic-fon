<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Professor details</title>
</head>
<body>
<%@ include file='home.jsp' %>
<table class="table table-striped">
<tbody>
 <tr>
	  
	  <th>First Name</th>
	  <th>Last Name</th>
	  <th>Email</th>
	  <th>Address</th>
	  <th>Phone</th>
	  <th>Reelection Date</th>
	  <th>City</th>
	  <th>Title</th>
	  </tr>
	  
	   <tr>
	  <td>${professorDto.firstName}</td>
	  <td>${professorDto.lastName}</td>
	  <td>${professorDto.email}</td>
	  <td>${professorDto.address}</td>
	  <td>${studentDto.phone}</td>
	  <td>${professorDto.reelectionDate}</td>
	  <td>${professorDto.cityDto.cityName}</td>
	  <td>${professorDto.titleDto.titleName}</td>
	  </tr>
	  </tbody>
	  </table>
</body>
</html>