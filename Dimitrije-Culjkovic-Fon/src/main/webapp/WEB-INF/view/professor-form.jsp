<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Professor - form</title>

<style>
.error {
	color: red
}
</style>
</head>
<body>
<%@ include file='home.jsp' %>
	<center>
		<h2>PROFESSOR FORM</h2>
	</center>
<div align="center">
	<form:form action="${pageContext.request.contextPath}/professor/save"
		modelAttribute="professorDto" method="post">
		
		<form:hidden path="id"/>
		<div style="background-color: rgba(223, 232, 223,0.5); width:50%;">
		<br> 
<table>
	<tbody>
	
	<tr>
	<td><label>First Name :</label></td>
	<td> <form:input path="firstName"/> </td>
	<td><form:errors path="firstName" cssClass="error" /></td>
	</tr>
	
	<tr>
	<td><label>Last Name :</label></td>
	<td> <form:input path="lastName"/> </td>
	<td><form:errors path="lastName" cssClass="error" /></td>
	</tr>
	
	<tr>
	<td><label>Email :</label></td>
	<td> <form:input path="email"/> </td>
	<td><form:errors path="email" cssClass="error" /></td>
	</tr>
	
	<tr>
	<td><label>Address :</label></td>
	<td> <form:input path="address"/> </td>
	<td><form:errors path="address" cssClass="error" /></td>
	</tr>
	
	<tr>
	<td><label>Phone :</label></td>
	<td> <form:input path="phone"/> </td>
	<td><form:errors path="phone" cssClass="error" /></td>
	</tr>
	
	<tr>
	<td><label>Reelection date :</label></td>
	<td> <form:input type="date" path="reelectionDate"/> </td>
	<td><form:errors path="reelectionDate" cssClass="error" /></td>
	</tr>
	
	
	<tr>
					<td><form:label path="cityDto">City</form:label></td>
					<td><form:select path="cityDto">
							<form:options items="${cities}" itemValue="id"
								itemLabel="cityName" />
						</form:select></td>
				</tr>
	

	
	<tr>
					<td><form:label path="titleDto">Title</form:label></td>
					<td><form:select path="titleDto">
							<form:options items="${titles}" itemValue="id"
								itemLabel="titleName" />
						</form:select></td>
				</tr>
	<tr>
	<td><label></label></td>
	<td> <input class="btn btn-primary" type="submit" value="Save" class= "save"> </td>
	</tr>
	
	</tbody>
	
	</table>
</div>

	</form:form> </div>
	
	<a href="/Dimitrije-Culjkovic-Fon/professor/home">Back to home page</a>
</body>
</html>