<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>All Exams</title>
</head>
<body>
<%@ include file='home.jsp' %>
<c:url var="home" value="/student/home"></c:url>


<div align="center" >

<h2> ALL APPLICATIONS </h2>

</div>

<input type = "button" value = "ADD APPLICATION" onclick="window.location.href='add'; return false" class="addButton btn btn-outline-secondary" />

<div align="center">
  <table class="table table-striped">
	  
	  <tr>
	  <th>Student</th>
	  <th>Exam</th>	
	  <th> Exam Date </th> 
	  
	  </tr>
	  
	  <c:forEach var="applicationDto" items = "${applications}">

	  <tr>
	  <td>${applicationDto.studentDto.firstName}  ${applicationDto.studentDto.lastName}</td>
	  <td>${applicationDto.examDto.subjectDto.name}</td>
	  <td> ${applicationDto.examDto.examDate } </td>
	 	 
	  </tr>
	  
	  </c:forEach>
	  
	  
	  </table> </div>
	  
	 <a href="${home}">Back to homepage</a> 
	  
</body>
</html>