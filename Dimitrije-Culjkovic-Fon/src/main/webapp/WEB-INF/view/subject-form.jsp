<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<meta charset="ISO-8859-1">
<title>Subject - form</title>

<style>
.error {
	color: red
}
</style>
</head>
<body>
<%@ include file='home.jsp' %>
	<c:url var="home" value="/subject/home"></c:url>
	<center>
		<h2>SUBJECT FORM</h2>
	</center>
<div align="center">
	<form:form action="${pageContext.request.contextPath}/subject/save"
		modelAttribute="subjectDto" method="post">
		
		<form:hidden path="id"/>
		 <div style="background-color: rgba(223, 232, 223,0.5); width:50%;">
<table>
	<tbody>
	
	<tr>
	<td><label> Name :</label></td>
	<td> <form:input path="name"/> </td>
	<td><form:errors path="name" cssClass="error" /></td>
	</tr>
	
	<tr>
	<td><label>Description :</label></td>
	<td> <form:input path="description"/> </td>
	
	</tr>
	
	<tr>
	<td><label>Year of study :</label></td>
	<td> <form:input path="yearOfStudy"/> </td>
	
	</tr>
	
	<tr>
	<tr>
	<td><form:label path="semester">Semester</form:label></td>
	<td><form:select path="semester">
	<form:options items="${semesters}" itemValue="semester"
				itemLabel="semester" />
						</form:select></td>
	</tr>
	
	
	
	<tr>
	<td><label></label></td>
	<td> <input class="btn btn-primary" type="submit" value="Save" class= "save"> </td>
	</tr>
	
	</tbody>
	
	</table>

 </div>
	</form:form> </div>
	
	<a href="${home}">Back to home page</a>
	
</body>
</html>