<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>All Subjects</title>
</head>
<body>
<%@ include file='home.jsp' %>
<c:url var="home" value="/subject/home"></c:url>
<div align="center" >

<h2> ALL SUBJECTS </h2>

</div>

<input type = "button" value = "ADD SUBJECT" onclick="window.location.href='add'; return false" class="addButton btn btn-outline-secondary" />

<div align="center">
  <table class="table table-striped">
	  
	  <tr>
	  
	  <th>Name</th>
	  <th>Description</th>
	  <th>Year of Study</th>
	  <th>Semester</th>
	  <th>Action</th>
	  </tr>
	  
	  <c:forEach var="subjectDto" items = "${subjects}">
	  
	  <c:url var="delete" value="/subject/delete" >
				<c:param name="id" value="${subjectDto.id}" />
			</c:url>
	  
	  <c:url var="update" value="/subject/update" >
	  <c:param name="id" value ="${subjectDto.id }"></c:param>
	   </c:url>
	   
	    <c:url var="details" value="/subject/details" >
	  <c:param name="id" value ="${subjectDto.id }"></c:param>
	   </c:url>
	   
	  <tr>
	  <td>${subjectDto.name}</td>
	  <td>${subjectDto.description}</td>
	  <td>${subjectDto.yearOfStudy}</td>
	  <td>${subjectDto.semester.semester}</td>
	  <td> <a href="${update}">Update</a> 
	  |
	       <a href="${delete}">Delete</a>
	  |
	  	   <a href="${details}">Details</a>     
	   </td>	 
	  </tr>
	  
	  </c:forEach>
	  
	  
	  </table> </div>
	  
	  <a href="${home}">Back to homepage</a>
</body>
</html>