<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>All Students</title>
</head>
<body>

<%@ include file='home.jsp' %>
<c:url var="home" value="/student/home"></c:url>

<br>
<div align="center" >

<h2> ALL STUDENTS </h2>

</div>
<br> <br>
<input type = "button" value = "ADD STUDENT" onclick="window.location.href='add'; return false" class="addButton btn btn-outline-secondary" />
<br> <br>

	  
	  <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Index number</th>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th> 
      <th scope="col">Email</th>
      <th scope="col">Address</th>
      <th scope="col">Phone</th>
      <th scope="col">Current Year Of Study</th>
      <th scope="col">City</th>
      <th scope="col">Action</th>
    </tr>
    
  </thead>
  <tbody>
    <tr>
      <c:forEach var="studentDto" items = "${ students}">
	  
	  <c:url var="delete" value="/student/delete" >
				<c:param name="id" value="${studentDto.id}" />
			</c:url>
	  
	  <c:url var="update" value="/student/update" >
	  <c:param name="id" value ="${studentDto.id }"></c:param>
	   </c:url>
	   
	    <c:url var="details" value="/student/details" >
	  <c:param name="id" value ="${studentDto.id }"></c:param>
	   </c:url>
	  <tr>
	  <td>${studentDto.indexNumber}</td>
	  <td>${studentDto.firstName}</td>
	  <td>${studentDto.lastName}</td>
	  <td>${studentDto.email}</td>
	  <td>${studentDto.address}</td>
	  <td>${studentDto.phone}</td>
	  <td>${studentDto.currentYearofStudy}</td>
	  <td>${studentDto.cityDto.cityName}</td>
	  <td> <a href="${update}">Update</a> 
	  |
	       <a href="${delete}">Delete</a>
	  |
	  		<a href="${details}">Details</a>     
	   </td>	 
	  </tr>
	  
	  </c:forEach>
    </tr>
  </tbody>
</table>
	  
	  
	  
	 <a href="${home}">Back to homepage</a> 
	  
</body>
</html>