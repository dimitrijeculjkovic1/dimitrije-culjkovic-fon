<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>All Professors</title>
</head>
<body>
<%@ include file='home.jsp' %>
<c:url var="home" value="/professor/home"></c:url>
<div align="center" >

<h2> ALL PROFESSORS </h2>

</div>
	  
	  <br> <br>
<input type = "button" value = "ADD PROFESSOR" onclick="window.location.href='add'; return false" class="addButton btn btn-outline-secondary" />
<br> <br>

	  
	  <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th> 
      <th scope="col">Email</th>
      <th scope="col">Address</th>
      <th scope="col">Phone</th>
      <th scope="col">Reelection Date</th>
      <th scope="col">City</th>
      <th scope="col">Title</th>
      <th scope="col">Action</th>
    </tr>
    
  </thead>
  <tbody>
    <tr>
      <c:forEach var="professorDto" items = "${professors}">
	  
	  <c:url var="delete" value="/professor/delete" >
				<c:param name="id" value="${professorDto.id}" />
			</c:url>
	  
	  <c:url var="update" value="/professor/update" >
	  <c:param name="id" value ="${professorDto.id }"></c:param>
	   </c:url>
	   
	   	  <c:url var="details" value="/professor/details" >
	  <c:param name="id" value ="${professorDto.id }"></c:param>
	   </c:url>
	   
	  <tr>
	  <td>${professorDto.firstName}</td>
	  <td>${professorDto.lastName}</td>
	  <td>${professorDto.email}</td>
	  <td>${professorDto.address}</td>
	  <td>${professorDto.phone}</td>
	  <td>${professorDto.reelectionDate}</td>
	  <td>${professorDto.cityDto.cityName}</td>
	  <td>${professorDto.titleDto.titleName}</td>
	  <td> <a href="${update}">Update</a> 
	  |
	       <a href="${delete}">Delete</a>
	  |    
	  	   <a href="${details}">Details</a>
	  		
	   </td>	 
	  </tr>
	  
	  </c:forEach>
    </tr>
  </tbody>
</table>
	  
	  <a href="${home}">Back to homepage</a>
</body>
</html>