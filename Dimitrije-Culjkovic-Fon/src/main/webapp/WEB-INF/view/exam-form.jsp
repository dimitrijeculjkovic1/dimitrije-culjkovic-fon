<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<meta charset="ISO-8859-1">
<title>Exam - form</title>

<style>
.errors {
	color: red
}
</style>
</head>
<body>
<%@ include file='home.jsp' %>
	<c:url var="home" value="/student/home"></c:url>
	<center>
		<h2>EXAM FORM</h2>
	</center>
	<div align="center">
		<form:form action="${pageContext.request.contextPath}/exam/save"
			modelAttribute="examDto" method="post">

			<form:hidden path="id" />
<div style="background-color: rgba(223, 232, 223,0.5); width:50%;">
			<table>
				<tbody>

					<tr>
						<td><form:label path="subjectDto">Subject</form:label></td>
						<td><form:select path="subjectDto">
								<form:options items="${subjects}" itemValue="id"
									itemLabel="name" />
							</form:select></td>
							<td><form:errors path="subjectDto" cssClass="errors" /></td>
					</tr>


					<tr>
						<td><form:label path="professorDto">Professor</form:label></td>
						<td><form:select path="professorDto">
								<form:options items="${professors}" itemValue="id"
									itemLabel="firstName" />
							</form:select></td>
					</tr>
					
					<tr>
	<td><label>Exam date :</label></td>
	<td> <form:input type="date" path="examDate" id = "id"/> </td>
	<td><form:errors path="examDate" cssClass="error" /></td>
	</tr>

					<tr>
						<td><label></label></td>
						<td> <input class="btn btn-primary" type="submit" value="Save" class= "save"> </td>
					</tr>

				</tbody>

			</table>

</div>
		</form:form>
	</div>

	<a href="${home}">Back to home page</a>

</body>
</html>