<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<title>Insert title here</title>
</head>
<body>

<c:url var="addStudent" value="/student/add"></c:url>
<c:url var="studentList" value="/student/list"></c:url>
<c:url var="addProfessor" value="/professor/add"></c:url>
<c:url var="professorList" value="/professor/list"></c:url>
<c:url var="addSubject" value="/subject/add"></c:url>
<c:url var="subjectList" value="/subject/list"></c:url>
<c:url var="addExam" value="/exam/add"></c:url>
<c:url var="examList" value="/exam/list"></c:url>
<c:url var="addApplication" value="/application/add"></c:url>
<c:url var="applicationList" value="/application/list"></c:url>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">STUDENT SERVICE</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="index.jsp">Home <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="${studentList}">Student</a>
      <a class="nav-item nav-link" href="${professorList}">Professor</a>
      <a class="nav-item nav-link" href="${examList}">Exam</a>
      <a class="nav-item nav-link" href="${subjectList}">Subject</a>
      <a class="nav-item nav-link" href="${applicationList}">Application</a>
      
    </div>
  </div>
</nav>

</body>
</html>