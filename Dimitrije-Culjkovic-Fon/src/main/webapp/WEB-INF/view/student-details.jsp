<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Details</title>
</head>
<body>
<%@ include file='home.jsp' %>
<br> <br>
<table class="table table-striped">

<tbody>
	<tr>
	  <th>Index number</th>
	  <th>First Name</th>
	  <th>Last Name</th>
	  <th>Email</th>
	  <th>Address</th>
	  <th>Phone</th>
	  <th>Current Year Of Study</th>
	  <th>City</th>
	  </tr>
	<tr>
	  <td>${studentDto.indexNumber}</td>
	  <td>${studentDto.firstName}</td>
	  <td>${studentDto.lastName}</td>
	  <td>${studentDto.email}</td>
	  <td>${studentDto.address}</td>
	  <td>${studentDto.phone}</td>
	  <td>${studentDto.currentYearofStudy}</td>
	  <td>${studentDto.cityDto.cityName}</td>
    	</tbody>
    	</table>

</body>
</html>