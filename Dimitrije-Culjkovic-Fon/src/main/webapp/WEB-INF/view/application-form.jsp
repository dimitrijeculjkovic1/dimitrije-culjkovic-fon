<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Exam - form</title>

<style>
.error {
	color: red
}
</style>
</head>
<body>
<%@ include file='home.jsp' %>
	<c:url var="home" value="/student/home"></c:url>
	<center>
		<h2>APPLICATION FORM</h2>
	</center>
	<div align="center">
		<form:form action="${pageContext.request.contextPath}/application/save"
			modelAttribute="applicationDto" method="post">

			<form:hidden path="id" />
			
			<div style="background-color: rgba(223, 232, 223,0.5); width:50%;">

			<table>
				<tbody>

					<tr>
						<td><form:label path="studentDto">Student</form:label></td>
						<td><form:select path="studentDto">
								<form:options items="${students}" itemValue="id"
									itemLabel="firstName" />
							</form:select></td>
							<td><form:errors path="studentDto" cssClass="error" /></td>
					</tr>


					<tr>
						<td><form:label path="examDto">Exam</form:label></td>
						<td><form:select path="examDto">
								<form:options items="${exams}" itemValue="id"
									itemLabel="subjectDto.name" />
							</form:select></td>
					</tr>

					<tr>
						<td><label></label></td>
						<td> <input class="btn btn-primary" type="submit" value="Save" class= "save"> </td>
					</tr>

				</tbody>

			</table>

</div>
		</form:form>
	</div>

	<a href="${home}">Back to home page</a>

</body>
</html>