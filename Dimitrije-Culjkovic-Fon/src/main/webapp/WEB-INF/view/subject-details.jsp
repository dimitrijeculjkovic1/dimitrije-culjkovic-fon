<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Subject details</title>
</head>
<body>
<%@ include file='home.jsp' %>
<c:url var="home" value="/subject/home"></c:url>
<table class="table table-striped">
	  
	  <tr>
	  
	  <th>Name</th>
	  <th>Description</th>
	  <th>Year of Study</th>
	  <th>Semester</th>
	  
	  </tr>
	  
	   <tr>
	  <td>${subjectDto.name}</td>
	  <td>${subjectDto.description}</td>
	  <td>${subjectDto.yearOfStudy}</td>
	  <td>${subjectDto.semester.semester}</td>
	  
	  </tr>
	  
	  
	  
	  </table>
	  <a href="${home}">Back to homepage</a>
</body>
</html>